# TFIDF on Metaflow Demo

A demo flow classifying spam messages using scikit-learn and Metaflow.

Uses a `conda` decorator to specify the steps' dependencies (required when run the flow remotely).
Includes the [Spam Text Message Classification](https://www.kaggle.com/team-ai/spam-text-message-classification) dataset from Kaggle.


## Installation

```
pip install tfidf --extra-index-url https://gitlab.com/api/v4/projects/23339513/packages/pypi/simple
```

## Run locally

```
python -m tfidf.flow --environment=conda run
```

## Run on Argo
Requires a k8s cluster with argo and the `metaflow-argo` plugin.

```
python -m tfidf.flow --environment=conda argo create
python -m tfidf.flow --environment=conda argo trigger
```
