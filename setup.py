from setuptools import setup

setup(
    name='tfidf',
    version='0.1.0',
    description='Demo Scikit-learn TFIDF on Metaflow',
    url='https://gitlab.com/romk/tfidf',
    license='MIT',
    author='romk',
    author_email='r0mk@gmx.net',
    install_requires=[
        'metaflow',
        'scikit-learn',
    ],
    packages=['tfidf'],
    data_files=[('tfidf', ['tfidf/spam-text-message-20170820.csv'])],
    include_package_data=True
)
