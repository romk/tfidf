from metaflow import FlowSpec, step, IncludeFile, Parameter, conda, conda_base

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report

import os


def full_path(filename):
    here = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(here, filename)


def preprocess(lang, texts):
    """
    Lemmatizes a text corpus using a spacy model.
    """
    tokens = [[t.text.lower() for t in doc if not t.is_punct and not t.is_stop] for doc in lang.pipe(texts)]
    return [' '.join(doc) for doc in tokens]


@conda_base(python='3.8',
            libraries={'pandas': '1.1.5', 'scikit-learn': '0.23.2'})
class TfIdfFlow(FlowSpec):
    """
    A flow to classify spam messages using scikit-learn.
    """
    text_messages = IncludeFile('text_messages',
                                is_text=True,
                                help='The path to a dataset file.',
                                default=full_path('spam-text-message-20170820.csv'))

    trees = Parameter('trees',
                      help='The number of trees in the forest (n_estimators).',
                      default=512)

    @step
    def start(self):
        """
        Parse the CSV file and split a dataset into training and test.
        """
        import io
        import pandas as pd

        df = pd.read_csv(io.StringIO(self.text_messages))
        df['Category'].replace(['ham', 'spam'], [0, 1], inplace=True)
        self.train, self.test = train_test_split(df, test_size=0.3, random_state=42)
        self.next(self.preprocess_train, self.preprocess_test)


    @conda(libraries={'spacy': '2.3.4'})
    @step
    def preprocess_train(self):
        """
        Lemmatize the training dataset.
        """
        from spacy.lang.en import English

        self.train_x = preprocess(English(), self.train['Message'])
        self.next(self.train_model)


    @conda(libraries={'spacy': '2.3.4'})
    @step
    def preprocess_test(self):
        """
        Lemmatize the test dataset.
        """
        from spacy.lang.en import English

        self.test_x = preprocess(English(), self.test['Message'])
        self.next(self.evaluate_model)


    @step
    def train_model(self):
        """
        Vectorize the text messages and train a model.
        """
        self.vectorizer = TfidfVectorizer()
        X = self.vectorizer.fit_transform(self.train_x)

        hyperparameters = {
            'n_estimators': [self.trees],
            'min_samples_split': [2, 5, 10, 20]
        }

        search = GridSearchCV(RandomForestClassifier(random_state=42), hyperparameters, n_jobs=-1)
        search.fit(X, self.train['Category'])

        self.model = search.best_estimator_
        self.score = search.best_score_
        self.parameters = search.best_params_

        self.next(self.evaluate_model)


    @step
    def evaluate_model(self, inputs):
        """
        Evaluate the trained model on the test dataset.
        """
        test_x = inputs.train_model.vectorizer.transform(inputs.preprocess_test.test_x)
        pred_y = inputs.train_model.model.predict(test_x)
        true_y = inputs.preprocess_test.test['Category']
        self.report = classification_report(true_y, pred_y, target_names=['ham', 'spam'])
        self.next(self.end)


    @step
    def end(self):
        """
        Print out some stats.
        """
        print(self.report)


if __name__ == '__main__':
    TfIdfFlow()
